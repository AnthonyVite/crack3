#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


int psearch(const void *t, const void *e);
int pcompare(const void *a, const void *b);

// Stucture to hold both a plaintext password and a hash.
typedef struct 
{
    // TODO: FILL THIS IN
    char plainpass[50];
    char hashpass[33];
}entry;

void printEntry(const entry *e)
{
    printf("%s %s", e->plainpass, e->hashpass);
}


// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
entry *read_dictionary(char *filename, int *size)
{
    entry *pass = malloc(100 * sizeof(entry));
    FILE *in = fopen(filename, "r");
    char line[100];
    //Variable to keep track of location in array
    int count = 0;
    int capacity = 100;
    while(fgets(line, PASS_LEN, in) != NULL)
    {
        //Trim new line
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';

        char *str = malloc(strlen(line)+1);
        strcpy(str, line);

        //Add plaintext to struct array
        sscanf(line, "%s", pass[count].plainpass);

        //Hash the password
        char *hash = md5(line, strlen(line));
        //Add hash to struct array
        sscanf(hash, "%s", pass[count].hashpass);

        count++;

        if ( count == capacity)
        {
            //Make array bigger
            capacity += 1000;
            pass = realloc(pass, capacity * sizeof(entry));
        }

        //Free some space
        free(hash);
    }
    fclose(in);
    *size = count;
    return pass;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int size;
    // TODO: Read the dictionary file into an array of entry structures
    entry *dict = read_dictionary(argv[2], &size);
    
    // TODO: Sort the hashed dictionary using qsort.
    qsort(dict, size, sizeof(entry), pcompare);
    // You will need to provide a comparison function that
    // sorts the array by hash value.

    // TODO
    // Open the hash file for reading.
    FILE *hf = fopen(argv[1], "r");
    if(!hf)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
        perror(NULL);
        exit(1);
    }

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    char findhash[33];

    //Variable for cracked passwords
    int cracked = 0;

    while(fgets(findhash, 30, hf) != NULL)
    {
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)

    //Trim character off of foundhash
    char *nl = strchr(findhash, '\n');
    if (nl) *nl = '\0';

        entry *foundhash = bsearch(findhash, dict, size, sizeof(entry), psearch);
        if(foundhash)
        {
            cracked++;
            printf("%d) Found!", cracked);
            printEntry(foundhash);
            printf("\n");
        }
    }
    free(dict);
    fclose(hf);
}

//Search for pass for qsort
int psearch(const void *t, const void *e)
{
    char * tt = (entry *)t;
    entry *ee = (entry *)e;

    return strcmp(((char *)tt)->hashpass, ((entry *)ee)->hashpass);
}

int pcompare(const void *a, const void *b)
{
    int cmp = strcmp(((entry *)a)->hashpass, ((entry *)b)->hashpass);
    return cmp;
}
